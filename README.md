# Monitoring a Web App on a Linode - Downtime Alerts & Recovery with Python

## Table of Contents

1. [About](#about)
2. [Requirements](#requirements)
3. [Steps](#steps)

## About

This Python script schedules a job that regularly pings a containerized Nginx app running on a Linode server, ensuring that the Nginx web server remains continuously up and accessible. If the app or Linode is down, the script restarts the server(s).

## Requirements

To run this script, you should know how to:

- install Python packages using pip package manager (Python 3 should already be locally installed).
- create a Linode server and Personal Access Token on the Linode cloud platform.
- create a SSH key pair to authenticate to the Linode. (Download and store the private key safely in your local .ssh directory.)
- pull and run a DockerHub image as a container.
- set (global or project-scoped) environment variables. In PyCharm, you can set/ scope environment vars for this project (see Step 8 below).

## Steps

1. Fork this repo.

2. Install the following:

- `pip install requests`
- `pip install paramiko`
- `pip install linode_api4`
- `pip install schedule`

### Linode setup

3. In the Linode UI, create an account. Create a Personal Access Token to use in Step 8.

4. Create a Linode and configure it to allow connection using your SSH private key. Example server specs: 2GB RAM, Debian OS, 1 CPU, 50GB storage. Note the Linode's Instance ID to use in Step 12.

5. Manually SSH into the Linode. Run `apt-get update`. Follow [the instructions to install Docker on Debian OS](https://docs.docker.com/engine/install/debian/#install-using-the-repository). Add the Docker apt repository to apt sources. Install the latest Docker packages.

### Nginx setup

6. Run `docker run -d -p 8080:80 nginx` to start the Nginx container on the Linode. Run `docker ps` to get the container ID to use in Step 11.

7. In the Linode UI, under the Network tab, you can find the public IP and DNS hostname for your Linode. Load the app URL `[server_IP/hostname]:8080` in the browser to see Nginx running.

### Python script changes

8. Set the following environment variables:

- `SENDER_EMAIL` with the value of your email.
- `EMAIL_PWD` with the value of your email password OR, more securely, an application password provided by your email client. For Gmail for example, create an app-specific password here: `myaccount.google.com/u/1/apppasswords`. You can delete the app-specific password after running the script.
- `RECEIVER_EMAIL` with the value of your email or another email account you can access.
- `API_TOKEN` with the value of your Linode account's Personal Access Token.

- If you're using the PyCharm IDE, go to the Current File dropdown, click the vertical 3 dots, and then click Run with parameters. Enter the variables and their values, and save to apply the values to your project.

9. On line 23, replace the first arg passed to the SMTP with your `host` (Gmail or another client).

10. On lines 45-46, replace the first and last args passed to the connect method with your Linode's public IP and the absolute local path to your SSH private key file. Python will SSH into the Linode as a `root` user.

11. On line 48, replace your Nginx container's ID in the `docker start` command.

12. On lines 61 and 67, replace the second arg passed to the load method with your Linode's Instance ID.

13. On line 79, replace the URL passed to the GET request with your Linode's URL that loads the Nginx app in your browser.

### Run the script!

14. Run `monitor.py`. For as long as the Nginx container and Linode are up-and-running, the app should load in your browser. Every 30 seconds the script will print `nginx up-and-running!` in your console.

### Trigger test alerts

15. Manually SSH into your Linode and `docker stop` the Nginx container. The script will print:

- `Emailing error notification...` to the console. Your `RECEIVER_EMAIL` account will get an email every 30 seconds with the subject line `nginx down! Restart asap! This message is sent from Python.`
- `Restarting Linode...` and reboot the Linode.
- `Restarting nginx container...` and restart the container.
- `nginx container restarted!`. The Nginx app should be back up in your browser.
