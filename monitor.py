from requests import get
from smtplib import SMTP
from os import environ
import paramiko
from linode_api4 import LinodeClient, Instance
from time import sleep
import schedule

# See Curr File dropdown > Curr File 3 dots > Run w/ params
APP_EMAIL = environ.get("SENDER_EMAIL")
# py-web-monitor's app-specific pwd created in Google acct
APP_PWD = environ.get("EMAIL_PWD")
RECIPIENT = environ.get("RECEIVER_EMAIL")
TOKEN = environ.get("API_TOKEN")
print(APP_EMAIL, APP_PWD, RECIPIENT, TOKEN)


def error_email():
    # print Exception
    # print(f"Error: ${e}")
    print("Emailing error notification...")
    # configure email provider/ port
    with SMTP("smtp.gmail.com", 587) as server:
        # encrypt unsecured SMTP connection
        server.starttls()
        # identifies Py app to Gmail server
        server.ehlo()
        # log into Gmail account
        server.login(APP_EMAIL, APP_PWD)
        # send error email
        msg = """\
        Subject: nginx down!\n
        Restart asap! This message is sent from Python."""
        server.sendmail(APP_EMAIL, RECIPIENT, msg)


def restart_container():
    print("Restarting nginx container...")
    # locally SSH to remote server to exec Docker/ any Linux cmds
    ssh_client = paramiko.SSHClient()
    # confirm adding host key
    ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    # host = remote server address
    # default port is 22 (SSH)
    ssh_client.connect("45.33.96.213", username="root",
                       key_filename="/Users/upasananatarajan/.ssh/id_rsa")
    # restart stopped nginx container
    [stdin, stdout, stderr] = ssh_client.exec_command("docker start 1926905adf18")
    print(stdout.readlines())
    # close SSH connection
    ssh_client.close()
    print("nginx container restarted!")


def restart_linode_and_container():
    # restart Linode server
    print("Restarting Linode...")
    # connect to account using Personal Access Token
    linode_client = LinodeClient(TOKEN)
    # connect to existing Linode Instance
    nginx_server = linode_client.load(Instance, 54182178)
    nginx_server.reboot()

    # check Linode is Running before continuing
    while True:
        # MUST load Linode Instance again for app to wait until it's Running
        nginx_server = linode_client.load(Instance, 54182178)
        if nginx_server.status == "running":
            # give 10 more secs for Linode to come up/ SSH connection to work
            sleep(10)
            # restart nginx container
            restart_container()
            break


# schedule all functionality to run hourly
def nginx_monitor():
    try:
        response = get("http://45-33-96-213.ip.linodeusercontent.com:8080/")
        print(response.status_code)
        if response.status_code == 200:
            print("nginx up-and-running!")
        else:
            # if non-200 response received
            error_email()
            # restart nginx container
            restart_container()
    except:
        # except Exception as e:
        # if no response
        # to test, stop nginx container
        error_email()
        # restart server AND container
        restart_linode_and_container()


# following is to test scheduler
schedule.every(30).seconds.do(nginx_monitor)
# schedule.every(60).minutes.do(nginx_monitor)

while True:
    schedule.run_pending()
